#
# MIT License
#
# Copyright (c) 2019 Keisuke Sehara
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

"""a test module for communicating with the FastEventServer."""

import sys as _sys
import os as _os
import struct as _struct
import threading as _threading
import socket as _socket
import eventcalls as _evt

API_MSG_SIZE = 2
DEFAULT_HOST = 'localhost'
DEFAULT_PORT = 11666 # may need to change according to service.cfg

def from_byte_to_int(byte):
    return int.from_bytes(byte, 'big')

def from_int_to_byte(val):
    return val.to_bytes(1, 'big')

class Commands(object):
    """the commands to send to the server."""
    SYNC  = b'\x10'
    EVENT = b'\x20'
    CLEAR = b'\x00'
    QUIT  = b'\x03'

class Packet:
    """a packet for FastEventServer API."""
    def __init__(self):
        self.__codec = _struct.Struct('Bc')
        self.__using = _threading.Lock()

    def encode(self, index, sync=None, event=None, shutdown=False):
        """returns a 2-byte command"""
        code = int.from_bytes(Commands.CLEAR, 'big')
        for flag, cmd in ((sync, Commands.SYNC),
                        (event, Commands.EVENT),
                        (shutdown, Commands.QUIT)):
            if flag == True:
                code |= from_byte_to_int(cmd)
        with self.__using:
            return self.__codec.pack(index, from_int_to_byte(code))

    def decode(self, msg):
        """returns (index, sync, event) in a tuple."""
        with self.__using:
            index, state = self.__codec.unpack(msg)
        code = from_byte_to_int(state)
        ret  = (index,)
        for cmd in (Commands.SYNC, Commands.EVENT):
            flag = from_byte_to_int(cmd)
            ret += ((code & flag != 0),)
        return ret

class Connection(_evt.EventHandler):
    def __init__(self, port=None, host=None, buffersize=API_MSG_SIZE):
        """initializes a connection with given service host and port."""
        if port is None:
            port = DEFAULT_PORT
        if host is None:
            host = DEFAULT_HOST
        self.__port    = _socket.socket(_socket.AF_INET, _socket.SOCK_DGRAM)
        self.__port.connect((host, port))
        self.__index   = 0
        self.__packet  = Packet()
        self.__reading = True
        self.__routine = _evt.Routine(_evt.io.DatagramIO(self.__port, buffersize=buffersize),
                                    self, start=True)
        self.__state   = dict(index=-1, sync=False, event=False)

    def __getattr__(self, name):
        if name in ('index', 'sync', 'event'):
            return self.__state[name]
        else:
            raise AttributeError(name)

    def __setattr__(self, name, value):
        if name in ('index', 'sync', 'event'):
            self.request(**{name: value})
        else:
            super().__setattr__(name, value)

    def initialized(self, evt=None):
        """called after the socket is opened.
        does not do anything under the default implementation."""
        pass

    def request(self, sync=None, event=None, shutdown=False):
        """sends a status-update request to the server."""
        if sync is None:
            sync  = self.sync
        if event is None:
            event = self.event
        try:
            self.__port.sendall(self.__packet.encode(self.__index,
                                    sync=sync, event=event, shutdown=shutdown))
            self.__index += 1
            if self.__index == 256:
                self.__index = 0
        except OSError:
            raise RuntimeError("FastEventServer socket seems to have been already closed for writing.")

    def handle(self, evt):
        """handle inputs from the server.

        `evt` consists of (bytes, address).
        default implementation decodes the message and calls updated().
        """
        index, sync, event = self.__packet.decode(evt[0])
        self.updated(index=index, sync=sync, event=event)

    def updated(self, index=-1, sync=None, event=None):
        """called whenever its state gets updated.

        intended for the use with the subclasses.
        default implementation prints the current state.
        """
        print(f"service state: index={index}, sync={sync}, event={event}")
        if _os.isatty(_sys.stdin.fileno()):
            print(">>> ", end='', flush=True)

    def done(self, evt=None):
        """called after the socket is closed.

        `evt` is not used.
        default implementation does nothing.
        """
        if evt is not None:
            print(evt)
        self.__reading = False

    def is_open(self):
        """returns True if the connection is still alive."""
        return self.__reading and self.__routine.is_running()

    def close(self, shutdown=True):
        """closes the underlying connection (if any)"""
        if shutdown == True:
            if self.is_open():
                self.request(shutdown=True)
        self.__routine.stop()
