#
# MIT License
#
# Copyright (c) 2019 Keisuke Sehara
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

"""remote-control tool for FastEvent-jAER."""

import eventcalls as _evt
import socket as _socket
import threading as _threading
import re as _re

DEFAULT_NAME   = 'python-client'
DEFAULT_HOST   = 'localhost'
DEFAULT_PORT   = 8997
DEFAULT_LISTEN = 12666
DEFAULT_BUFSIZ = 1024

DEBUG_NOTIFICATION = False
DEBUG_CONNECTION   = False

class TransactionError(RuntimeError):
    def __init__(self, msg):
        RuntimeError.__init__(self, msg)

class ProxyHandler:
    def __init__(self, name, conn, entries):
        self.__name    = name
        self.__conn    = conn
        self.__entries = entries

    def __getattr__(self, name):
        if name in self.__entries.keys():
            typ = self.__entries[name]
            buf = self.__conn.request(f"get {self.__name}.{name}", wait=True)
            if typ is None:
                return eval(buf)
            else:
                return typ(buf)
        else:
            raise AttributeError(name)

    def __setattr__(self, name, value):
        if name.startswith('_ProxyHandler'):
            super().__setattr__(name, value)
        elif name in self.__entries:
            self.__conn.request(f"set {self.__name}.{name} {value}")
        else:
            super().__setattr__(name, value)

    def __repr__(self):
        return f"<ProxyHandler '{self.__name}' for {self.__conn}>"

class LoggingHandler:
    def __init__(self, conn):
        self.__conn    = conn
        self.__logging = False

    def __getattr__(self, name):
        if name == 'status':
            return self.__logging
        else:
            raise AttributeError(name)

    def __setattr__(self, name, value):
        if name == 'status':
            if isinstance(value, bool):
                self.__set(value)
            else:
                raise ValueError(f"logging status must be boolean, got {type(value)}")
        else:
            super().__setattr__(name, value)

    def start(self):
        return self.__set(True)

    def stop(self):
        return self.__set(False)

    def __set(self, value):
        if value == True:
            return self.__conn.request(f"logging start", wait=True)
        else:
            return self.__conn.request("logging stop", wait=True)
        self.__logging = value

    def __repr__(self):
        return f"<LoggingHandler for {self.__conn}>"

class Notification(_evt.EventHandler):
    """notifies the current settings."""

    def __init__(self, parent, debug=DEBUG_NOTIFICATION):
        self.__registered = []
        self.__assign     = _re.compile(r'([a-zA-Z0-9_\.]+)=(.+)')
        self.__action     = _re.compile(r'([a-zA-Z0-9_]+)\((.*)\)')
        self.__debug      = debug

    def register(self, listener):
        """registers a listener object.

        a listener object must have the `update(name, param)` and
        `action(name, param)` methods.
        """
        self.__registered.append(listener)

    def unregister(self, listener):
        if listener in self.__registered:
            del self.__registered[self.__registered.index(listener)]

    def initialized(self, evt=None):
        """called after the socket is opened.
        with the default implementation, it does nothing."""
        pass

    def done(self, evt=None):
        """called after the socket is closed.

        `evt` is not used.
        default implementation does nothing.
        """
        pass

    def handle(self, evt):
        """handle inputs from the server.

        `evt` consists of (bytes, address).
        default implementation prints the response (under debug=True condition).
        """
        msg = evt[0][:-2].decode('utf-8').strip()
        is_assignment = self.__assign.match(msg)
        is_action     = self.__action.match(msg)
        if is_assignment:
            name  = is_assignment.group(1)
            param = eval(is_assignment.group(2))
            self.log(f"assign: {name} --> {param}")
            for listener in self.__registered:
                listener.update(name, param)
        elif is_action:
            name  = is_action.group(1)
            param = is_action.group(2)
            if len(param) > 0:
                param = eval(param)
            self.log(f"action: {name} << {param}")
            for listener in self.__registered:
                listener.action(name, param)
        else:
            self.log(f"does not match: {msg}")

    def log(self, msg):
        if self.__debug == True:
            print(msg)

class EventListener:
    """the interface that can be a listener for Notification object."""
    def update(self, name, param):
        """called when `param` is assigned to the config `name`."""
        pass

    def action(self, name, param):
        """called when an action `name` occurred with the parameter being `param`."""
        pass

class EventLogger(EventListener):
    """an EventListener subclass that only prints every event."""
    def update(self, name, param):
        """called when `param` is assigned to the config `name`."""
        print(f"assign: {name} --> {param}")

    def action(self, name, param):
        """called when an action `name` occurred with the parameter being `param`."""
        print(f"action: {name} << {param}")

class Connection(_evt.EventHandler):
    """a connection to the remote-control port to FastEvent-jAER."""
    def __init__(self, name=None, host=None, port=None, listening=None,
                buffersize=None, show_responses=True):
        if name is None:
            name = DEFAULT_NAME
        if host is None:
            host = DEFAULT_HOST
        if port is None:
            port = DEFAULT_PORT
        if listening is None:
            listening = DEFAULT_LISTEN
        if buffersize is None:
            buffersize = DEFAULT_BUFSIZ
        self.__name         = name
        self.__portnum      = port
        self.__port         = _socket.socket(_socket.AF_INET, _socket.SOCK_DGRAM, _socket.IPPROTO_UDP)
        self.__port.connect((host, port))
        self.__target       = ProxyHandler('target', self, dict(type=str,
                                                x=int, y=int, width=int, height=int))
        self.__experiment   = ProxyHandler('exp', self, dict(name=str))
        self.show_responses = show_responses

        self.__iolock       = _threading.Lock()
        self.__request      = _threading.Condition(self.__iolock)
        self.__response     = _threading.Condition(self.__iolock)
        self.__waiting      = False
        self.__buffersize   = buffersize
        self.__buffer       = None
        self.__listening    = listening
        self.__errorpat     = _re.compile(r'error: (.+)')
        self.__logging      = LoggingHandler(self)
        self.__notifier     = Notification(self)
        self.__is_open      = False
        self.__closing      = False
        self.__comerror     = False
        self.__subscribed   = None

        self.setup()

    def __debug(self, msg, end="\n"):
        if DEBUG_CONNECTION == True:
            print(msg, end=end, flush=True)

    def setup(self):
        self.__debug("opening a connection to the client...", end='')
        self.__routine      = _evt.Routine(_evt.io.DatagramIO(self.__port,
                                                    buffersize=self.__buffersize,
                                                    port=self.__portnum),
                                            self, start=True)
        self.__is_open      = True
        self.__debug("done.")

        self.__debug("setting up a listening port...", end='')
        self.__subscribed   = _evt.Routine(_evt.io.DatagramIO.bind(self.__listening,
                                                            buffersize=self.__buffersize),
                                                self.__notifier, start=True)
        self.__debug("done.")

        self.__debug("starting up the communication with the host...", end='')

        try:
            self.request("set target.type Rectangle", wait=True)
            self.request(f"subscribe {self.__name} {self.__listening}", wait=True, ignore_error=True)
        except TransactionError:
            print("***failed to communicate with server")
            self.close()
        self.__debug("done.")

    def __del__(self):
        self.close()

    def __repr__(self):
        return f"jAER.Connection({id(self)})"

    def __setattr__(self, name, value):
        if name == 'note':
            self.request(f"set note {value}")
        elif name == 'mode':
            self.request(f"set mode {value}")
        elif name == 'logging':
            self.__logging.status = value
        else:
            super().__setattr__(name, value)

    def __getattr__(self, name):
        if name == 'target':
            return self.__target
        elif name == 'exp':
            return self.__experiment
        elif name == 'note':
            return str(self.request("get note", wait=True))
        elif name == 'mode':
            return str(self.request("get mode", wait=True))
        elif name == 'name':
            return self.__name
        elif name == 'logging':
            return self.__logging
        elif name == 'notification':
            return self.__notifier
        else:
            raise AttributeError(name)

    def initialized(self, evt=None):
        """called after the socket is opened.
        with the default implementation, it does nothing."""
        pass

    def request(self, msg, wait=True, ignore_error=False):
        """sends a command to jAER."""
        try:
            with self.__response:
                self.__port.sendall(msg.encode('utf-8'))
                if wait == True:
                    self.__waiting = True
                    self.__response.wait()
                    self.__waiting = False
                    if isinstance(self.__buffer, RuntimeError) and (ignore_error == False):
                        raise self.__buffer
                    return self.__buffer
        except OSError as e:
            if self.__closing == True:
                pass
            else:
                print(f"***failed to perform a request: {e}")
                self.close()

    def handle(self, evt):
        """handle inputs from the server.

        `evt` consists of (bytes, address).
        default implementation prints the response, if
        'wait' is not set and show_responses is set True.
        """
        with self.__response:
            self.__buffer = evt[0][:-3].decode('utf-8')
            is_error = self.__errorpat.match(self.__buffer)
            if self.__waiting == True:
                if is_error:
                    self.__buffer = RuntimeError(is_error.group(1))
                self.__response.notify_all()
            elif is_error:
                print(f"*** {is_error.group(1)}")
            elif self.show_responses == True:
                print(self.__buffer)

    def done(self, evt=None):
        """called after the socket is closed.

        `evt` is not used.
        default implementation does nothing.
        """
        if self.__waiting == True:
            with self.__response:
                self.__buffer = TransactionError("unexpected I/O error")
                self.__response.notify_all()

    def is_open(self):
        """returns True if the connection is still alive."""
        return self.__routine.is_running() and self.__is_open

    def close(self):
        """closes the underlying connection (if any)"""
        if self.__is_open == False:
            return

        self.__debug("closing the connection...")
        self.__closing = True
        if self.__logging.status == True:
            self.__debug("stoping logging...")
            self.logging = False
        if self.__routine.is_running():
            if self.__subscribed:
                self.__debug("unsubscribing...")
                self.request(f"unsubscribe {self.__listening}", wait=True)
        self.__debug("closing the main connection...")
        self.__routine.stop()
        if self.__subscribed:
            self.__debug("closing the notification connection...")
            self.__subscribed.stop()
        self.__is_open = False
