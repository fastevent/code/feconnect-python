#
# MIT License
#
# Copyright (c) 2019 Keisuke Sehara
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

import sys as _sys
from datetime import date as _date

DEBUG = False
def debug(msg, end='\n'):
    if DEBUG == True:
        print(msg, file=_sys.stderr, end=end, flush=True)

MAX_BORDER_POS = 240

try:
    from pyqtgraph.Qt import QtCore, QtWidgets, QtGui
    from .. import Connection as _Connection, EventListener as _EventListener

    session = None

    class InitializationError(RuntimeError):
        def __init__(self, msg):
            RuntimeError.__init__(self, msg)

    class App(QtWidgets.QWidget):
        def __init__(self, parent=None):
            super().__init__(parent=parent)

        def startSession(self):
            global session
            session = SessionGUI()
            session.show()

    class SessionGUI(QtWidgets.QWidget, _EventListener):
        receivedMode            = QtCore.pyqtSignal(str)
        receivedTarget          = QtCore.pyqtSignal(dict)
        receivedExperimentName  = QtCore.pyqtSignal(str)
        receivedLogFileName     = QtCore.pyqtSignal(str)
        receivedLoggingState    = QtCore.pyqtSignal(bool)
        receivedError           = QtCore.pyqtSignal(str)
        receivedRunningNote     = QtCore.pyqtSignal(str)
        requestWindowTitle      = QtCore.pyqtSignal(str)

        def __init__(self, parent=None):
            super().__init__(parent=parent)
            self.__logging  = False
            self.__expname  = ""
            self.__logfile  = ""

            layout = QtWidgets.QGridLayout()
            self.setLayout(layout)
            self.exp         = ExperimentEditor()
            self.modes       = EvaluationModeSelector()
            self.target      = TargetEditor()
            self.current     = CurrentExperimentDisplay()
            self.logging     = LoggingManager()

            self.target.parameterUpdating.connect(self.requestTargetUpdate)
            self.modes.modeUpdating.connect(self.requestModeUpdate)
            self.exp.experimentNameChanging.connect(self.requestExperimentNameUpdate)
            self.logging.loggingStateChanging.connect(self.requestLoggingUpdate)

            self.receivedExperimentName.connect(self.updateWithExperimentName)
            self.receivedExperimentName.connect(self.exp.updateWithRemote)
            self.receivedExperimentName.connect(self.current.setCurrentExperiment)
            self.receivedTarget.connect(self.target.updateWithRemote)
            self.receivedMode.connect(self.modes.updateWithRemote)
            self.receivedLoggingState.connect(self.exp.updateWithLoggingState)
            self.receivedLoggingState.connect(self.logging.updateWithRemote)
            self.receivedLogFileName.connect(self.current.setCurrentFilename)
            self.receivedRunningNote.connect(self.current.setRunningNote)
            self.requestWindowTitle.connect(self.setWindowTitle)

            layout.addWidget(self.current, 0, 0, 1, 2)
            layout.addWidget(self.exp,   1, 0, 1, 2)
            layout.addWidget(self.modes, 2, 0)
            layout.addWidget(self.target, 2, 1)
            layout.addWidget(self.logging, 3, 0, 1, 2)

            self.__connection = _Connection()
            if not self.__connection.is_open():
                raise InitializationError("cannot open connection to FastEvent-jAER")
            self.__connection.notification.register(self)

            QtCore.QCoreApplication.instance().aboutToQuit.connect(self.shutdown)
            self.initializeWithRemote()

        def shutdown(self):
            self.__connection.close()

        def __setattr__(self, name, value):
            if name == 'logging_state':
                self.__logging = value
                debug(f"...SessionGUI: emit: receivedLoggingState({value})")
                self.receivedLoggingState.emit(value)
            elif name == 'logging_file':
                self.__logfile = value
                debug(f"...SessionGUI: emit: receivedLogFileName('{value}')")
                self.receivedLogFileName.emit(value)
            else:
                super().__setattr__(name, value)


        def update(self, name, param):
            """called when `param` is assigned to the config `name`."""
            debug(f">>> update(name='{name}', param='{param}')")
            if name == 'target':
                self.receivedTarget.emit(param)
            elif name == 'mode':
                self.receivedMode.emit(param)
            elif name == 'exp.name':
                self.receivedExperimentName.emit(param)
            elif name == 'note':
                self.receivedRunningNote.emit(param)

        def action(self, name, param):
            """called when an action `name` occurred with the parameter being `param`."""
            print(f">>> action(name='{name}', param='{param}')")
            if name == 'startLogging':
                self.updateWithLoggingState(True, param)
            elif name == 'endLogging':
                self.updateWithLoggingState(False, "")
            elif name == 'error':
                self.receivedError.emit(param)

        def initializeWithRemote(self):
            self.exp.updateWithRemote(self.__connection.exp.name)
            self.target.updateWithRemote(dict((edge, getattr(self.__connection.target, edge)) \
                                        for edge in ('x', 'y', 'width', 'height')))
            self.modes.updateWithRemote(self.__connection.mode)
            self.logging_state = self.__connection.logging.status
            if self.__logging == True:
                self.logging_file = "unknown"
            else:
                self.__logfile = ""
            self.updateWithExperiment()
            self.receivedRunningNote.emit(self.__connection.note)

        def updateWithLoggingState(self, status, logfile):
            self.logging_state = status
            self.logging_file  = logfile
            self.updateWithExperiment()
            debug(f"...SessionGUI: done: updateWithLoggingState({status}, '{logfile}')")

        def requestTargetUpdate(self, param, value):
            setattr(self.__connection.target, param, value)

        def requestModeUpdate(self, value):
            self.__connection.mode = value

        def requestLoggingUpdate(self, value):
            self.__connection.logging = value

        def requestExperimentNameUpdate(self, name):
            self.__connection.exp.name = name

        def requestRunningNoteUpdate(self, msg):
            self.__connection.note = msg

        def updateWithExperiment(self):
            debug(f"...SessionGUI: updateWithExperiment: logging-state={self.__logging}, expname='{self.__expname}'")
            status = "IDLE" if self.__logging == False else "RUNNING"
            self.requestWindowTitle.emit(f"({status}) {self.__expname}")

        def updateWithExperimentName(self, name):
            self.__expname = name
            self.updateWithExperiment()

    class EvaluationModeSelector(QtWidgets.QGroupBox):
        modeUpdating = QtCore.pyqtSignal(str)

        labels   = ('Estimate', 'Evaluate', 'Trigger')
        commands = tuple(lab.lower() for lab in labels)

        def __init__(self, parent=None):
            super().__init__(parent=parent)
            self.setTitle("FastEvent mode")
            layout = QtWidgets.QHBoxLayout()
            self.setLayout(layout)
            self.selector = QtWidgets.QComboBox()
            self.selector.setEditable(False)
            for lab in self.labels:
                self.selector.addItem(lab)
            self.selector.currentTextChanged.connect(self.setMode)
            layout.addWidget(self.selector)
            self.__updating = True
            self.selector.setEnabled(False)

        def setMode(self, mode):
            if self.__updating == False:
                cmd = mode.lower()
                self.__updating = True
                self.selector.setEnabled(False)
                self.modeUpdating.emit(cmd)

        def updateWithRemote(self, mode):
            self.selector.setCurrentIndex(self.commands.index(mode.lower()))
            self.__updating = False
            self.selector.setEnabled(True)

    class TargetEditor(QtWidgets.QGroupBox):
        parameterUpdating      = QtCore.pyqtSignal(str, int)
        receivedTargetPosition = QtCore.pyqtSignal(str, int)

        labels   = ('Top', 'Bottom', 'Left', 'Right')
        commands = tuple(lab.lower() for lab in labels)

        def __init__(self, parent=None):
            super().__init__(parent=parent)
            self.setTitle("Target specs")
            self.__updating = False
            self.__target   = dict(x=0, y=0, width=0, height=0)
            layout = QtWidgets.QGridLayout()
            self.setLayout(layout)
            EDGE_ROW = 0
            POSITION_ROW = 1
            LABEL_COL = 0
            EDITOR_COL = 1

            self.edgelab = QtWidgets.QLabel("Edge:")
            self.edgesel = QtWidgets.QComboBox()
            self.edgesel.setEditable(False)
            for lab in self.labels:
                self.edgesel.addItem(lab)
            self.__edge     = self.edgesel.currentText().lower()
            self.edgesel.currentTextChanged.connect(self.setEdgeOfInterest)
            layout.addWidget(self.edgelab, EDGE_ROW, LABEL_COL, alignment=QtCore.Qt.AlignRight)
            layout.addWidget(self.edgesel, EDGE_ROW, EDITOR_COL)

            self.poslab  = QtWidgets.QLabel("Position:")
            self.posedit = QtWidgets.QLineEdit()
            self.posedit.setValidator(QtGui.QIntValidator(0, MAX_BORDER_POS))
            self.posedit.setText('0')
            self.posedit.editingFinished.connect(self.updateWithPositionEditor)
            layout.addWidget(self.poslab, POSITION_ROW, LABEL_COL, alignment=QtCore.Qt.AlignRight)
            layout.addWidget(self.posedit, POSITION_ROW, EDITOR_COL)
            layout.setColumnStretch(LABEL_COL, 1)
            layout.setColumnStretch(EDITOR_COL, 2)
            self.__updating = True
            self.setEnabled(False)

        def setEnabled(self, value):
            for item in (self.edgelab, self.edgesel,
                        self.poslab, self.posedit):
                item.setEnabled(value)

        def setEdgeOfInterest(self, edge):
            if self.__updating == False:
                cmd = edge.lower()
                if cmd != self.__edge:
                    self.__updating = True
                    self.__edge     = cmd
                    self.posedit.setText(str(self.position()))
                    self.__updating = False

        def updateWithPositionEditor(self):
            if self.__updating == False:
                value = int(self.posedit.text())
                self.__updating = True
                self.setEnabled(False)
                self.parameterUpdating.emit(*(self.edgeUpdate(value)))

        def updateWithRemote(self, target):
            self.__target   = target
            self.posedit.setText(str(self.position()))
            self.__updating = False
            self.setEnabled(True)
            self.receivedTargetPosition.emit(self.__edge, self.position())

        def position(self):
            if self.__edge == 'top':
                return self.__target['y'] + self.__target['height']
            elif self.__edge == 'bottom':
                return self.__target['y']
            elif self.__edge == 'left':
                return self.__target['x']
            elif self.__edge == 'right':
                return self.__target['x'] + self.__target['width']
            else:
                return -1

        def setPosition(self, position):
            self.posedit.setText(str(position))
            self.updateWithPositionEditor()

        def edgeUpdate(self, position):
            if self.__edge == 'top':
                return 'height', position - self.__target['y']
            elif self.__edge == 'bottom':
                return 'y', position
            elif self.__edge == 'left':
                return 'x', position
            elif self.__edge == 'right':
                return 'width', position - self.__target['x']
            else:
                return '', 0

    class ExperimentEditor(QtWidgets.QGroupBox):
        INITIAL_NAME_FMT       = "Untitled_session%Y-%m-%d-001"
        experimentNameChanging = QtCore.pyqtSignal(str)

        def __init__(self, parent=None):
            super().__init__(parent)
            self.setTitle("Experiment")
            layout = QtWidgets.QGridLayout()
            self.setLayout(layout)
            self.__requesting = False

            self.namefmt_lab    = QtWidgets.QLabel("Name format:")
            self.namefmt_edit   = QtWidgets.QLineEdit(self.INITIAL_NAME_FMT)
            self.name_lab       = QtWidgets.QLabel("Name output:")
            self.name_output    = QtWidgets.QLabel("")
            self.namefmt_edit.returnPressed.connect(self.updateWithEditor)
            self.namefmt_edit.setMinimumWidth(220)
            self.namefmt_edit.setSizePolicy(QtWidgets.QSizePolicy(
                                    QtWidgets.QSizePolicy.MinimumExpanding,
                                    QtWidgets.QSizePolicy.Minimum))
            self.__updating     = False
            self.__namefmt      = self.INITIAL_NAME_FMT # current name format
            self.__init_date = False
            self.__name         = None

            layout.addWidget(self.namefmt_lab,  0, 0, alignment=QtCore.Qt.AlignRight)
            layout.addWidget(self.namefmt_edit, 0, 1)
            layout.addWidget(self.name_lab,     1, 0, alignment=QtCore.Qt.AlignRight)
            layout.addWidget(self.name_output,  1, 1)
            layout.setColumnStretch(0, 1)
            layout.setColumnStretch(1, 2)
            self.__requesting = True
            self.setEnabled(False)

        def setEnabled(self, value):
            for item in (self.namefmt_lab, self.namefmt_edit,
                        self.name_lab, self.name_output,
                        ):
                item.setEnabled(value)
            super().setEnabled(value)

        def updateWithRemote(self, name):
            if name == self.__name:
                pass
            else:
                # TODO: parse name
                print("***experiment name parse functionality to be implemented")
                pass
            self.__requesting = False
            self.setEnabled(True)

        def updateWithEditor(self):
            if self.__requesting == False:
                if self.__updating == False:
                    namefmt = self.namefmt_edit.text()
                    if (len(namefmt.strip())) == 0:
                        # revert to the current value
                        self.__updating   = True
                        self.namefmt_edit.setText(self.__namefmt)
                        self.__updating   = False
                        return
                    elif (namefmt != self.__namefmt) or (self.__init_date == False):
                        self.__updating   = True
                        self.__namefmt    = namefmt
                        self.__name       = _date.today().strftime(namefmt)
                        self.name_output.setText(self.__name)
                        self.__updating   = False
                        self.__init_date  = True
                        self.__requesting = True
                        self.experimentNameChanging.emit(self.__name)

        def updateWithLoggingState(self, status):
            debug(f"...ExperimentEditor: updateWithLoggingState({status})")
            self.setEnabled(not status)


    class CurrentExperimentDisplay(QtWidgets.QGroupBox):
        def __init__(self, parent=None):
            super().__init__(parent=parent)
            self.setTitle("Current status")
            layout = QtWidgets.QGridLayout()
            self.setLayout(layout)
            self.expname_lab  = QtWidgets.QLabel("Experiment name:")
            self.expname_out  = QtWidgets.QLabel()
            self.filename_lab = QtWidgets.QLabel("Log file name:")
            self.filename_out = QtWidgets.QLabel()
            self.status_lab   = QtWidgets.QLabel("Running note status:")
            self.status_out   = QtWidgets.QLabel()
            self.error_lab    = QtWidgets.QLabel("jAER status:")
            self.error_out    = QtWidgets.QLabel()

            layout.addWidget(self.expname_lab,  0, 0, alignment=QtCore.Qt.AlignRight)
            layout.addWidget(self.expname_out,  0, 1)
            layout.addWidget(self.filename_lab, 1, 0, alignment=QtCore.Qt.AlignRight)
            layout.addWidget(self.filename_out, 1, 1)
            layout.addWidget(self.status_lab,   2, 0, alignment=QtCore.Qt.AlignRight)
            layout.addWidget(self.status_out,   2, 1)
            layout.addWidget(self.error_lab,    3, 0, alignment=QtCore.Qt.AlignRight)
            layout.addWidget(self.error_out,    4, 1)
            layout.setColumnStretch(0, 1)
            layout.setColumnStretch(1, 3)

        def setCurrentExperiment(self, name):
            self.expname_out.setText(name)

        def setCurrentFilename(self, name):
            self.filename_out.setText(name)

        def setRunningNote(self, note):
            self.status_out.setText(note)

    class LoggingManager(QtWidgets.QWidget):
        LAB_START = "Start logging"
        LAB_STOP  = "Stop logging"
        loggingStateChanging = QtCore.pyqtSignal(bool)

        def __init__(self, parent=None):
            super().__init__(parent=parent)
            self.__updating = False
            self.action = QtWidgets.QPushButton(self.LAB_START)
            self.action.setCheckable(True)
            self.action.setChecked(False)
            self.action.toggled.connect(self.updateWithButtonState)
            layout = QtWidgets.QHBoxLayout()
            self.setLayout(layout)
            layout.addStretch(3)
            layout.addWidget(self.action, 1)

        def updateWithButtonState(self, state):
            if self.__updating == False:
                debug(f"...LoggingManager: updateWithButtonState({state})")
                self.__updating = True
                self.loggingStateChanging.emit(state)

        def updateWithRemote(self, state):
            debug(f"...LoggingManager: updateWithRemote({state})")
            if self.__updating == False:
                self.__updating = True
                self.action.setChecked(state)
            if state == True:
                self.action.setText(self.LAB_STOP)
            else:
                self.action.setText(self.LAB_START)
            self.__updating = False

except ImportError:
    print("***could not load Qt library: consider installing pyqtgraph with PyQt5.")
