
def main():
    try:
        from pyqtgraph.Qt import QtGui
        from . import SessionGUI, InitializationError
    except ImportError:
        print("***could not load Qt library: consider installing pyqtgraph with PyQt5.")
        return

    try:
        mainapp = QtGui.QApplication([])
        session = SessionGUI()
        session.show()
        mainapp.exec()
    except InitializationError as e:
        print(f"***{e}")
