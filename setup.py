import setuptools
from feconnect import VERSION_STR

setuptools.setup(
    name='feconnect',
    version=VERSION_STR,
    description='tools to connect to APIs of FastEvent-jAER and FastEventServer',
    url='https://gitlab.com/fastevent/feconnect-python',
    author='Keisuke Sehara',
    author_email='keisuke.sehara@gmail.com',
    license='MIT',
    install_requires=[
        'eventcalls>=1.0',
        ],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        ],
    packages=setuptools.find_packages(),
    entry_points={
        'console_scripts': ['feclient=feconnect.jaer.gui.routines:main']
    }
)
